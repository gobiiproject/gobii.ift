(ns ift.digest.v1
  (:require
   [ift.util :refer [dissoc-in]]))

(def ^:private renames
  {[:metadata :dataSetOrientationType] :datasetOrientationType
   [:metadata :dataSet] :dataset
   [:metadata :dataSetType] :datasetType})

(defn- json->metadata
  [json metadata-keys]
  (select-keys (first json) metadata-keys))

(defn- json->instructions
  [json instruction-keys]
  (into []
    (map
      #(select-keys % instruction-keys)
      json)))

(defn- handle-special-case
  [json loc reducer]
  (if (vector? json)
    (reduce reducer (mapv #(handle-special-case % loc reducer) json))
    (if (= (count loc) 0)
      json
      (handle-special-case (get json (first loc)) (rest loc) reducer))))

(defn- handle-special-cases
  [json special-cases]
  (into {}
    (map (fn [[loc reducer]]
           [(last loc)
            (handle-special-case json loc reducer)]))
    special-cases))

(defn- handle-removal
  [json removal]
  (if (vector? json)
    (mapv #(handle-removal % removal) json)
    (if (= (count removal) 1)
      (dissoc json (first removal))
      (update json (first removal) handle-removal (rest removal)))))

(defn- handle-removals
  [json removals]
  (reduce handle-removal json removals))

(defn- handle-rename
  [json location name]
  (let [value (get-in json location)]
    (-> json
      (dissoc-in location)
      (assoc-in (assoc location (dec (count location)) name) value))))

(defn- handle-renames
  [json renames]
  (reduce
    (fn [acc [location name]] (handle-rename acc location name))
    json
    renames))

(defn- remove-nils
  "remove pairs of key-value that has
   nil value from a (possibly nested) map.
   also transform map to nil if all of its
   value are nil"
  [nm]
  (clojure.walk/postwalk
   (fn [el]
     (if (map? el)
       (into {} (remove (comp nil? second) el))
       el))
   nm))

(def ^:private metadata-keys
  [:project :platform :dataSet :experiment
   :mapset :gobiiCropType :jobPayloadType
   :contactEmail :contactId :gobiiJobStatus
   :gobiiFile])

(def ^:private instruction-keys
  [:table :gobiiFileColumns])

; qcCheck is true if any instruction has it set to true
; datasetType and datasetOrientationType are set to the last non null value
; special cases are reducer functions, specifically for metadata
(def ^:private special-cases
  {[:qcCheck] #(or %1 %2)
   [:gobiiFileColumns :dataSetType] #(or %2 %1)
   [:gobiiFileColumns :dataSetOrientationType] #(or %2 %1)})

(def removals
  [[:instructions :gobiiFileColumns :dataSetType]
   [:instructions :gobiiFileColumns :dataSetOrientationType]])

(defn parse
  [m]
  (let [m (remove-nils m)
        meta (json->metadata m metadata-keys)
        special-cases (handle-special-cases m special-cases)
        _ (println (merge meta special-cases))
        instructions (json->instructions m instruction-keys)
        m {:metadata (merge meta special-cases)
           :instructions instructions}]
    (-> m
      (handle-removals removals)
      (handle-renames renames)
      (update-in [:metadata :datasetType] (fn [n] {:id nil :name n})))))
