(ns ift.digest.v3)


(defmulti translate|gobiiFileColumnType :gobiiColumnType)

(defmethod translate|gobiiFileColumnType "CSV_COLUMN"
  [{:keys [rCoord cCoord]}]
  ["COLUMN" {:row rCoord :column cCoord}])

(defmethod translate|gobiiFileColumnType "CSV_ROW"
  [{:keys [rCoord cCoord]}]
  ["ROW" {:row rCoord :column cCoord}])

(defmethod translate|gobiiFileColumnType "CSV_BOTH"
  [{:keys [rCoord cCoord]}]
  ["MATRIX" {:row rCoord :column cCoord}])

(defmethod translate|gobiiFileColumnType "COLUMN"
  [{:keys [rCoord cCoord]}]
  ["COLUMN" {:row rCoord :column cCoord}])

(defmethod translate|gobiiFileColumnType "CONSTANT"
  [{:keys [constantValue]}]
  constantValue)

(defmethod translate|gobiiFileColumnType "AUTO_INCREMENT"
  [{:keys [rCoord cCoord]}]
  ["RANGE" 1])

(defmethod translate|gobiiFileColumnType "AUTOINCREMENT"
  [{:keys [rCoord cCoord]}]
  ["RANGE" 1])

(defn translate|gobiiFileColumn
  [{:keys [name] :as gobiiFileColumn}]
  {name (translate|gobiiFileColumnType gobiiFileColumn)})

(defn translate|instruction
  [{:keys [table gobiiFileColumns]}]
  {table
   (into {} (map translate|gobiiFileColumn gobiiFileColumns))})

(defn translate
  [{:keys [instructions]}]
  {:aspects
   (into {} (map translate|instruction instructions))})
