(ns ift.extract.v1)

(def instruction->metadata #{:gobiiCropType :contactId :contactEmail})
(def datasetextract->metadata #{:extractDestinationDirectory})

(defn parse
  [m]
  {:metadata (merge (select-keys (first m) instruction->metadata)
                    (select-keys (-> m first :dataSetExtracts first) datasetextract->metadata))
   :instructions (->> m
                      (map #(apply dissoc % instruction->metadata))
                      (map (fn [inst] (update inst :dataSetExtracts
                                              (fn [dse]
                                                (map #(apply dissoc % datasetextract->metadata)
                                                     dse))))))})
