(ns ift.core
  (:require
    #?(:clj [clojure.data.json :as json])
    [clojure.string :as string]
    [ift.digest.v1 :as digestV1]
    [ift.digest.v2 :as digestV2]
    [ift.digest.v3 :as digestV3]
    [ift.extract.v1 :as extractV1]
    [ift.extract.v2 :as extractV2])
  #?(:clj (:gen-class)))

(def protocols
  {:digest
   {:v1 {:parse digestV1/parse}
    :v2 {:parse digestV2/translate
         :translate digestV2/translate}
    :v3 {:translate digestV3/translate}}
   :extract
   {:v1 {:parse extractV1/parse}
    :v2 {:parse extractV2/parse
         :translate extractV2/translate}}})

(defn convert
  [type from to instruction]
  (let [parser (get-in protocols [type from :parse])
        translator (get-in protocols [type to :translate])]

    (assert (not (nil? parser)) (str "Parsing for " type "(" from ")" " files is not yet supported"))
    (assert (not (nil? translator)) (str "Translating to " type "(" from ")" " files is not yet supported"))

    (translator (parser instruction))))

(defn read-json
  [str]
  #?(:clj (json/read-str str :key-fn keyword)
     :cljs (js->clj (.parse js/JSON str) :keywordize-keys true)))

(defn write-json
  [m]
  #?(:clj (with-out-str (json/pprint m))
     :cljs (.stringify js/JSON (clj->js m) js/undefined 4)))

(defn translate
  [type from to str]
  (->> str
       read-json
       (convert type from to)
       write-json))

#?(:clj
   (defn -main
     [& [usage type from to src dst]]
     (let [[d e] (->> usage rest (map str) (map keyword) vec)
           type (keyword (string/lower-case type))
           from (keyword (string/lower-case from))
           to   (keyword (string/lower-case to))
           dst (if (= d :s) src dst)
           dst (if (and (= d e :f) (nil? dst)) src dst)
           writer (condp = e :f #(spit dst %) :s println)]
       (->> (condp = d :f src :s *in*)
            slurp
            (translate type from to)
            writer))))
