(defproject gobii/ift "0.1.0-SNAPSHOT"
  :description "Instruction file translator for Gobii digester and extractor"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/data.json "1.0.0"]]
  :main ^:skip-aot ift.core
  :target-path "target/%s"

  :profiles
    {:uberjar {:aot :all}
     :dev {:dependencies [[org.clojure/tools.namespace "0.3.0-alpha4"]
                          [criterium "0.4.4"]]
           :plugins [[lein-doo "0.1.10"]]}}

  :doo
    {:build "test"
     :alias {:default [:node]}}

  :cljsbuild
    {:builds
     [{:id           "test"
       :source-paths ["src/main/clojure" "src/test/clojure" "src/test/cljs"]
       :compiler     {:target        :nodejs
                      :language-in   :ecmascript5
                      :optimizations :none}}]})
